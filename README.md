# WordsmithFrontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.4.

## Requirements

For building and running the application you need:
- [Node.js](https://nodejs.org/en/)
- [Angular CLI](https://cli.angular.io/)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

By default the app will use `http://localhost:8080` base URL for backend calls. Be sure to change it in `enviroment.ts` if the backend is running at a different address.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. 

Use the `-prod` flag for a production build. Be sure to change API url in `environment.prod.ts`.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
