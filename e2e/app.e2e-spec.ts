import { browser, by, element } from 'protractor';

describe('wordsmith-frontend App', () => {

  const firstSentence = 'Hello, world!';
  const firstResult = 'olleH, dlrow!';

  const secondSentence = 'Just another sentence...';
  const secondResult = 'tsuJ rehtona ecnetnes...';

  it('should redirect to handler', () => {
    browser.get('/');
    expect(browser.getCurrentUrl()).toContain('/handler');
  });

  it('should process sentence', () => {
    element(by.id('sentence')).sendKeys(firstSentence);
    element(by.id('process')).click();
    expect(element(by.id('result')).getAttribute('value')).toEqual(firstResult);
  });

  it('should navigate to latest results', () => {
    element(by.id('results-link')).click();
    expect(browser.getCurrentUrl()).toContain('/latest');
  });

  it('should get added sentence and result', () => {
    const sentences = element.all(by.id('sentence'));

    browser.wait(function () {
      return sentences.count().then(function (countValue) {
        return countValue > 0;
      });
    }, 5000);

    const latest = sentences.first().all(by.tagName('td'));
    expect(latest.get(0).getText()).toEqual(firstSentence);
    expect(latest.get(1).getText()).toEqual(firstResult);
  });

  it('should navigate to handler', () => {
    element(by.id('handler-link')).click();
    expect(browser.getCurrentUrl()).toContain('/handler');
  });

  it('should process one more sentence', () => {
    element(by.id('sentence')).sendKeys(secondSentence);
    element(by.id('process')).click();
    expect(element(by.id('result')).getAttribute('value')).toEqual(secondResult);
  });

  it('should return to latest results', () => {
    element(by.id('results-link')).click();
    expect(browser.getCurrentUrl()).toContain('/latest');
  });

  it('should get latest sentence and result', () => {
    const sentences = element.all(by.id('sentence'));

    browser.wait(function () {
      return sentences.count().then(function (countValue) {
        return countValue > 0;
      });
    }, 5000);

    const latest = sentences.first().all(by.tagName('td'));
    expect(latest.get(0).getText()).toEqual(secondSentence);
    expect(latest.get(1).getText()).toEqual(secondResult);
  });
});
