import {  TestBed } from '@angular/core/testing';
import { LatestSentencesComponent } from './latest-sentences.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { SentenceService } from '../shared/service/sentence.service';
import { Sentence } from '../shared/entity/sentence';
import { asyncData } from '../testing/async-observable-helpers';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('LatestSentencesComponent', () => {

  const sentences: Sentence[] = [{content: 'abc', reversed: 'cba'}, {content: '123', reversed: '321'}];

  let component: LatestSentencesComponent;
  let service: SentenceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SentenceService],
      declarations: [LatestSentencesComponent],
      imports: [HttpClientTestingModule, RouterTestingModule, FormsModule]
    });

    component = TestBed.createComponent(LatestSentencesComponent).componentInstance;
    service = TestBed.get(SentenceService);

    spyOn(service, 'getLatest').and.returnValue(asyncData(sentences));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call service', () => {
    component.ngOnInit();
    expect(service.getLatest).toHaveBeenCalledTimes(1);
  });
});
