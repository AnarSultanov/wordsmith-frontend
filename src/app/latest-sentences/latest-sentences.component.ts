import { Component, OnInit } from '@angular/core';
import { SentenceService } from '../shared/service/sentence.service';
import { Sentence } from '../shared/entity/sentence';

@Component({
  selector: 'app-sentences',
  templateUrl: './latest-sentences.component.html',
  styleUrls: ['./latest-sentences.component.css']
})
export class LatestSentencesComponent implements OnInit {

  sentences: Sentence[];

  constructor(private sentenceService: SentenceService) {
  }

  ngOnInit() {
    this.loadLatest();
  }

  loadLatest(): void {
    this.sentenceService.getLatest().subscribe(s => this.sentences = s);
  }

}
