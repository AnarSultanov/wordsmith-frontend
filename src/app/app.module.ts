import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { SentenceHandlerComponent } from './sentence-handler/sentence-handler.component';
import { SentenceService } from './shared/service/sentence.service';
import { LatestSentencesComponent } from './latest-sentences/latest-sentences.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const appRoutes: Routes = [
  { path: 'handler', component: SentenceHandlerComponent },
  { path: 'latest', component: LatestSentencesComponent },
  { path: '',
    redirectTo: '/handler',
    pathMatch: 'full'
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    SentenceHandlerComponent,
    LatestSentencesComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule],
  providers: [SentenceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
