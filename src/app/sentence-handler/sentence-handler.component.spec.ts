import { TestBed } from '@angular/core/testing';

import { SentenceHandlerComponent } from './sentence-handler.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { SentenceService } from '../shared/service/sentence.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { asyncData } from '../testing/async-observable-helpers';
import { Sentence } from '../shared/entity/sentence';

describe('SentenceHandlerComponent', () => {
  const sentence: Sentence = { content: 'abc', reversed: 'cba' } as Sentence;

  let component: SentenceHandlerComponent;
  let service: SentenceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SentenceService],
      declarations: [SentenceHandlerComponent],
      imports: [HttpClientTestingModule, RouterTestingModule, FormsModule]
    });

    component = TestBed.createComponent(SentenceHandlerComponent).componentInstance;
    service = TestBed.get(SentenceService);
    spyOn(service, 'process').and.returnValue(asyncData(sentence));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call service', () => {
    component.processSentence();
    expect(service.process).toHaveBeenCalledTimes(1);
  });
});
