import { Component, OnInit } from '@angular/core';
import { SentenceService } from '../shared/service/sentence.service';
import { Sentence } from '../shared/entity/sentence';

@Component({
  selector: 'app-sentence',
  templateUrl: './sentence-handler.component.html',
  styleUrls: ['./sentence-handler.component.css']
})
export class SentenceHandlerComponent implements OnInit {

  sentence: Sentence = new Sentence();

  constructor(private sentenceService: SentenceService) {
  }

  ngOnInit() {
  }

  processSentence(): void {
    this.sentenceService.process(this.sentence).subscribe(s => this.sentence = s);
  }
}
