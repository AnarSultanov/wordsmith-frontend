import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Sentence } from '../entity/sentence';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class SentenceService {

  constructor(private http: HttpClient) {
  }

  process(sentence: Sentence): Observable<Sentence> {
    return this.http.post<Sentence>(environment.apiUrl + '/sentences/process', sentence);
  }

  getLatest(): Observable<Sentence[]> {
    return this.http.get<Sentence[]>(environment.apiUrl + /sentences/);
  }
}
