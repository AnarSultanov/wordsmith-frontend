import { TestBed, } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';

import { SentenceService } from './sentence.service';
import { Sentence } from '../entity/sentence';
import { environment } from '../../../environments/environment';

describe('SentenceService', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let sentenceService: SentenceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SentenceService],
      imports: [HttpClientTestingModule, RouterTestingModule]
    });
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    sentenceService = TestBed.get(SentenceService);
  });

  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpTestingController.verify();
  });

  describe('#process', () => {
    it('should process sentence', () => {

      const sentence: Sentence = { content: 'abc', reversed: undefined };
      const processed: Sentence = { content: 'abc', reversed: 'cba' };

      sentenceService.process(sentence).subscribe(
        data => expect(data).toEqual(processed, 'should return processed sentence'),
        fail
      );

      const req = httpTestingController.expectOne(environment.apiUrl + '/sentences/process');
      expect(req.request.method).toEqual('POST');
      expect(req.request.body).toEqual(sentence);

      // Expect server to return the hero after PUT
      const expectedResponse = new HttpResponse(
        { status: 200, statusText: 'OK', body: processed });
      req.event(expectedResponse);
    });
  });

  describe('#getLatest', () => {

    const expectedSentences: Sentence[] =
      [{content: 'abc', reversed: 'cba'}, {content: '123', reversed: '321'}];

    it('should return expected sentences (called once)', () => {

      sentenceService.getLatest().subscribe(
        sentences => expect(sentences).toEqual(expectedSentences, 'should return expected sentences'),
        fail
      );

      const req = httpTestingController.expectOne(environment.apiUrl + '/sentences/');
      expect(req.request.method).toEqual('GET');
      req.flush(expectedSentences);
    });

    it('should be OK returning no sentences', () => {

      sentenceService.getLatest().subscribe(
        heroes => expect(heroes.length).toEqual(0, 'should have empty sentence array'),
        fail
      );

      const req = httpTestingController.expectOne(environment.apiUrl + '/sentences/');
      expect(req.request.method).toEqual('GET');
      req.flush([]); // Respond with no heroes
    });
  });
});
